


<?php
const format = 'PR';


function getLevel($memberships){
    if(count($memberships)){
        $id = [];
        foreach ($memberships  as $membership) {
            # code...
            array_push($id, $membership->level_id);
        }
    }
    $levels = \App\Level::whereIn('id',$id)->get();
    $lev = '';
    if(count($levels)){

        foreach ($levels as $level) {
            # code...
            $lev .= ucwords($level->name). ', ';
        }
        return $lev;
    }
    return ;
}


function getDateFormat($string,$format){
        echo \Carbon\Carbon::parse($string)->format($format);
}

function when($string){
    echo \Carbon\Carbon::parse($string)->diffForHumans();
}


function checkIfBought($id){
    $isPaid = \App\TipPayment::where('user_id', auth()->id())->where('tip_id',$id)->first();

    if(isset($isPaid))
        return true;

    return false;
}

function createPaymentRecord($user,$amount, $type,$status =null){
    $payment = new \App\Payment;
    $ref = generatePaymentReference(format);
    return $payment->createRecord($user,$ref,$amount,$type, $status);

}


function  generatePaymentReference($format){
    $time = strtotime(\Carbon\Carbon::now());


    do {
        $pseudo = rand(1000, 4000);
    } while (strlen((string)$pseudo) < 4);

    $pseudoRegistrationNumber = $format. $pseudo . $time;

    return $pseudoRegistrationNumber;
}

function getImageLink($link){
    return env('ADMIN_URL').'/storage/'.$link;
}

function currency_format($amt){
    $eur = ceil($amt/25);
    echo $amt . ' CZK | '. $eur . ' EUR';
}

function getPackageName($id){
    $package = \App\Package::find($id);
    if($package)
    echo $package->name;
}



?>

