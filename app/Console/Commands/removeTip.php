<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class removeTip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tips:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes expired tips';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $now = \Carbon\Carbon::now();
        //get all tips
        $tips =\App\Tip::whereDate('end_date','<=', $now)
        ->where('status', 1)

        ->update(['status' => 0]);


    }
}
