<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    //
    public function create($request)
    {
        # code...
        // $this->level_id = $request->level;
        $this->package_id = $request->package;
        $this->name = $request->name;
        $this->course = $request->course;
        $this->date = $request->date;
        $this->time = $request->time;
        $this->tip = $request->tip;
        $this->confidence = $request->confidence;
        $this->end_date = $request->end_date;

        $this->single_purchase_tip = $request->single?1:0;
        if(!empty($request->single))
            $this->amount = $request->amount;
        return $this->save();
    }

    public function edit($request)
    {
        # code...
        // $this->level_id = $request->level;
        $this->package_id = $request->package;
        $this->name = $request->name;
        $this->course = $request->course;
        $this->date = $request->date;
        $this->time = $request->time;
        $this->tip = $request->tip;
        $this->confidence = $request->confidence;
        $this->end_date = $request->end_date;

        $this->single_purchase_tip = $request->single?1:0;
        if(!empty($request->single))
        $this->amount = $request->amount;
        return $this->save();
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function removePhoto()
    {
        # code...
        $this->photo = null;
        $this->save();
    }

}
