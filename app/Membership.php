<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    //
    public function create($request){
        $this->user_id = auth()->id();
        $this->payment_id = $request->payment;
        $this->level_id = $request->level;
        return $this->save();
    }

    public function level()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }
    public function payment()
    {
        return $this->belongsTo('App\Level', 'payment_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public function tips(){
        return $this->level->latestTips();
    }

}
