<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    public function create($request, $type)
    {
        # code...
        $this->user_id = $request->user_id;
        $this->amount = $request->amount;
        $this->type = $type;
        return $this->save();
    }

    public function getBalance($id)
    {
        # code...
        return $this->pluck('amount')->sum();
    }
}
