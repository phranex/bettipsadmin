<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    public function block(){
        $this->blocked = 1;
        $this->save();
    }


    public function unblock(){
        $this->blocked = 0;
        $this->save();
    }

    public function memberships()
    {
        return $this->hasMany('App\Membership', 'user_id')->where('status',1)->latest();
    }

    public function activeMembership()
    {
        return $this->hasMany('App\Membership', 'user_id')
                ->where('status', 1);
    }
}
