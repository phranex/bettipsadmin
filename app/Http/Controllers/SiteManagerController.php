<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteManagerController extends Controller
{
    //
    public function index()
    {
        # code...
        $settings = \App\Setting::all();
        $contents = [];
        if(count($settings)){


            foreach($settings as $setting){
                if($setting->name != 'enabler'){
                    $contents[$setting->name] = unserialize($setting->value);
                }
                
            }
            $banners_ =  \App\Setting::where('name','banner')->get();
            $banners = [];
            if($banners_){
                foreach ($banners_ as  $banner) {

                    $ban['id'] = $banner->id;
                    $ban['content'] = unserialize($banner->value);
                    array_push($banners, $ban);
                }
            }


            // foreach($settings as $setting){
            //     if(array_key_exists($setting->name, $contents)){
            //         if(gettype($contents[$setting->name]) == 'string'){
            //             $value = $contents[$setting->name];
            //             $contents[$setting->name] = [];
            //             array_push($contents[$setting->name],$value);
            //             array_push($contents[$setting->name],unserialize($setting->value));
            //         }else{
            //             array_push($contents[$setting->name],unserialize($setting->value));
            //         }
            //     }else{
            //         $contents[$setting->name] = unserialize($setting->value);

            //     }
            // }

        }
        return view('site.index', compact('contents','banners'));
    }
}
