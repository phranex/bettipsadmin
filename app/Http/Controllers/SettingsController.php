<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Setting;
use Illuminate\Support\Facades\Storage;


class SettingsController extends Controller
{
    //
    public function indexPage(Request $request)
    {
        # code...
        $request->validate([
            'banner-title' => 'required',
            'about-us-subtitle' => 'required',
            'about-us-description' => 'required',
            'why-use-us-subtitle' => 'required',
            'membership-plan-subtitle' => 'required',
            'why-use-us-subtitle-reason2' => 'required',
            'why-use-us-subtitle-reason1' => 'required',
            'why-use-us-subtitle-reason3' => 'required',
            'footer-description' => 'required',


        ]);

        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'index';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Index Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Index Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');
        // /print($s);

    }

    public function how_it_works_page(Request $request)
    {

        # code...
        $request->validate([
            'how-it-works-subtitle' => 'required',
            'how-it-works-description' => 'required',
            'how-it-works-step1' => 'required',
            'how-it-works-step2' => 'required',
            'how-it-works-step3' => 'required',



        ]);

        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }

        $data = serialize($data);
        $settings = new Setting;
        $name = 'hiw';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','How it works Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','How it works Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');
        // /print($s);

    }


    public function instruction(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'instruction';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function payments(Request $request)
    {
        # code...


        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'payments';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Automatic Payments successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function terms(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'terms';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function personal(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'personal';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function guarantee(Request $request)
    {
        # code...

        $request->validate([
            'editordata' => 'required',

        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'guarantee';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Instruction successfully created');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Instructions successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }

    public function contact_page(Request $request)
    {
        # code...
        $request->validate([

            'phone-number' => 'required',
            'email' => 'required',



        ]);


        foreach ($request->except('_token') as $key => $value) {
            $data[$key] = $value;
        }
        $data = serialize($data);
        $settings = new Setting;
        $name = 'contact';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','Contact Us Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','Contact Us Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }


    public function faq_page(Request $request)
    {
        # code...
        $index = 0;
        $loop = 1;

        foreach ($request->except('_token') as $key => $value) {
            if($loop%2){
                $questions[$loop] = $value;
            }else{
                $answers[$loop] = $value;
            }
            $loop++;
        }
        $result = array_combine($questions,$answers);

        $data = serialize($result);


        $settings = new Setting;
        $name = 'faq';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','FAQ Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','FAQ Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }
    public function hiw_page(Request $request)
    {
        # code...
        $index = 0;
        $loop = 1;

        foreach ($request->except('_token') as $key => $value) {
            if($loop%2){
                $questions[$loop] = $value;
            }else{
                $answers[$loop] = $value;
            }
            $loop++;
        }
        $result = array_combine($questions,$answers);

        $data = serialize($result);


        $settings = new Setting;
        $name = 'how';
        $name_exists =  Setting::where('name',$name)->first();
        if($name_exists){
            $name_exists->value = $data;
            $name_exists->save();
            return back()->with('success','How it works Page successfully updated');
        }
        elseif($settings->create($name, $data)){

            return back()->with('success','How it works Page successfully updated');
        }

        return back()->with('error', 'An unexpected error occurred. Please try again');


    }
    public function uploadBanner(Request $request)
    {
        # code...
        // return request()->all();
        $captions = $request->except(['photo','_token']);

        $request->validate([
            'photo[].*' => 'required|image|array|max:2000'
        ]);
        $num = 0;
        $paths = [];
        $banners = [];
        $index = 0;

        if(request()->hasFile('photo')){
            if(count(request()->file('photo'))){
                foreach(request()->file('photo') as $file){
                    if($file){
                        $directory = 'uploads/banner';
                        $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file

                            //dd($name);
                        $fileName = "banner_".$name.'.'.$file->extension();
                        $path = $file->storeAs($directory,$fileName);
                        array_push($paths,$path);

                    }else {
                        continue;
                    }


                }

                $banner_index = 0;
                $banner_items = $request->except(['_token','photo']);
                $banner_items = array_values($banner_items);

                for( $k = 0; $k < count($paths); $k++){
                    $num  = $k * 2;


                    $banner = [];
                    $banner['caption'] = $banner_items[$num];
                    $banner['path'] = $paths[$k];
                    $banner['caption-small'] =  $banner_items[$num+1];
                    array_push($banners,$banner);
                }

                foreach($banners as $banner){
                    $setting = new Setting;
                    $path = serialize($banner);
                    $name = 'banner';
                    $setting->create($name, $path);
                }



                return back()->with('success', 'Banners uploaded successfully');
            }

        }

       return back()->with('error', 'Banners could not be saved');
    }

    public function deleteBanner($banner)
    {
        # code...

        if(is_numeric($banner)){
            $banner = Setting::find($banner);

            if($banner){
                //$banner = unserialize($banner);
                $ban = unserialize($banner->value);
                Storage::delete($ban['path']);
                $banner->delete();
                return back()->with('success', 'Banner deleted successfully');
            }
        }
        return back()->with('error', 'Banner could not be deleted');
    }


}
