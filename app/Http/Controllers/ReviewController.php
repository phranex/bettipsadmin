<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ReviewController extends Controller
{
    //
    public function index()
    {
        # code...
        $reviews = \App\Review::all();

        return view('review.index', compact('reviews'));
    }

    public function store(Request $request)
    {
        # code...
        $request->validate([
            'photo' => 'required'
        ]);

        $files = request()->except(['_token']);
        if(request()->hasFile('photo')){
           foreach (request()->file('photo') as $file) {
               # code...
               $directory = 'uploads/reviews';
               $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
               $fileName = "review_".$name.'.'.$file->extension();
               $path = $file->storeAs($directory,$fileName);
               $review = new \App\Review;
               $review->path = $path;
               if(!empty($request->instagram)){
                    $review->isInstagram = 1;
                    $review->fullname = $request->fullname;
                    $review->title = $request->title;
                    $review->description = $request->description;

                }
               $review->save();

           }
           return back()->with('success', 'Uploaded  successfully');
        }
        return back()->with('error', 'Unexpected error occurred');
    }

    public function deletePhoto($id)
    {
        # code...
        $review = \App\Review::find($id);
        if($review){
            //$path = getImageLink($tip->photo);
            Storage::delete($review->path);

            $review->delete();
            return back()->with('success', 'Review removed successfully');
        }
        return back()->with('error', 'An unexpected error occurred');
    }
}
