<?php

namespace App\Http\Controllers;

use App\Library\Consume;
use App\Library\Paypal\Paypal;
use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $packages = \App\Package::all();
        // get all products on paypal
        return view('package.index',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);
        $package = new \App\Package;
        if($package->create(request('name'), request('description'))){
             //add package as a product on paypal
            $paypal_product = $this->createPackageOnPayPal($request, $package);
            if(!$paypal_product)
                return back()->with('success', 'Package created successfully but could not be added on paypal.');

            return back()->with('success', 'Package created successfully');
        }
        return back()->with('error', 'An unexpected error occurred');
    }

    public function storeOnPayPal($id)
    {
        # code...
        $package = Package::find($id);
        if(!$package) return back()->with('error','Unexpected error occurred');
        $product = $this->createPackageOnPayPal($package, $package);
        if($product)
           return back()->with('success', 'Added on Paypal successfully');

        return back()->with('error', 'Error occurred. Please try again');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'package_id' => 'required'
        ]);
        $package = \App\Package::find($request->package_id);
        if($package){
            if($package->edit(request('name'), request('description'))){
                return back()->with('success', 'Package created successfully');
            }
        }

        return back()->with('error', 'An unexpected error occurred');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        if(is_numeric(request('id'))){
            $package = \App\Package::find(request('id'));
            if($package->remove()){
                return back()->with('success', 'Package deleted successfully');
            }

            return back()->with('error', 'An unexpected error occurred');
        }
    }
    public function getLevels()
    {
        # code...
        if(is_numeric(request('id'))){
            $package = \App\Package::find(request('id'));
            if($package){
                logger($package->levels);
                return array('status' => 1, 'data' => $package->levels);
            }

            return array('status' => 0, 'data' => []);
        }
    }

    public function showLevelOnHomepage($id)
    {
        # code...
        if(is_numeric(request('id'))){
            $package = \App\Package::find(request('id'));
            if($package->show()){
                return back()->with('success', 'Package updated successfully');
            }

            return back()->with('error', 'An unexpected error occurred');
        }
    }

    public function createPackageOnPayPal($request, $package)
    {
        # code...
        try {
            //code...
            Paypal::getAccessToken();
            $client = Consume::getInstance();
            // return $client->getHeaders();
            $url = 'catalogs/products';
            $data =  [
                'name' => $request->name,
                'description' => $request->description,
                "type" => "SERVICE",
                "category" => "SOFTWARE",

            ];
            $response = $client->getResponseViaCurl('POST',$url,json_encode($data, true));
            if($response->id){
                $package->product_id = $response->id;
                $package->save();

            }
            return true;
        } catch (\Throwable $th) {
            //throw $th;
            logger($th);
            return false;
        }

    }
}
