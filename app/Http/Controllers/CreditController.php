<?php

namespace App\Http\Controllers;

use App\Credit;
use Illuminate\Http\Request;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'amount' => 'numeric|required',
            'user_id' => 'numeric|required',

        ]);

        //dd($request);
        if($request->amount > 0)
            $transaction_type = 'Credit';
        else
            $transaction_type = 'Debit';
        $type = 'Top up Transaction: '. $transaction_type;
        $payment = createPaymentRecord($request->user_id,$request->amount,$type, 'completed');
        if($payment){
            $credit = new \App\Credit;
            if($credit->create($request, $type)){
                return back()->with('success', 'Credit was updated successfully');
            }
            $payment->delete();

        }

        return back()->with('error', 'An unexpected error occurred');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Credit  $credit
     * @return \Illuminate\Http\Response
     */
    public function show(Credit $credit)
    {
        //
        return $credit->getBalance($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Credit  $credit
     * @return \Illuminate\Http\Response
     */
    public function edit(Credit $credit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Credit  $credit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Credit $credit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Credit  $credit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credit $credit)
    {
        //
    }
}
