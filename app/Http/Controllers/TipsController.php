<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class TipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tips = \App\Tip::where('status',1)->latest()->paginate(10);
        $tips_ = \App\Tip::where('status',1)->latest()->paginate(10);
        $grouped_tips = [];
        $tips_num = count($tips);
        if($tips_num){

            foreach($tips as $tip){
                 $package = \App\Package::find($tip->package_id);
                if(!isset($grouped_tips[$package->name])){


                         $grouped_tips[$package->name] = array();
                }
                array_push($grouped_tips[$package->name],$tip);
            }

        }

        //dd($grouped_tips);
        $packages = \App\Package::all();
        return view('tips.index', compact('grouped_tips','tips_','packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $request->validate([
            // 'level' => "required|numeric",
            'package' => "required|numeric",
            'name' => "required",
            'course' => "required",
            'date' => "required",
            'end_date' => 'required',
            'time' => "required",
            'confidence' => "required",

        ]);

        $tip = new \App\Tip;
        if($tip->create($request)){

            return back()->with('success', 'Tip created successfully');
        }

        return back()->with('error', 'An unexpected error occurred.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //

        $request->validate([
            'package' => "required|numeric",
            // 'level' => "required|numeric",
            'name' => "required",
            'course' => "required",
            'date' => "required",
            'time' => "required",
            'confidence' => "required",

        ]);
        if(is_numeric(request('id'))){
            $tip = \App\Tip::find(request('id'));
            if($tip){
                $tip->edit($request);
                return back()->with('success', 'Tip edited successfully');
            }

            return back()->with('error', 'An unexpected error occurred.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'level_id' => "required|numeric",
            'name' => "required",
            'course' => "required",
            'name' => "required",
            'course' => "required",
            'name' => "required",
            'course' => "required",
        ]);

        $tip = new \App\Tip;
        if($tip->create()){

            return back()->with('success', 'Tip created successfully');
        }

        return back()->with('error', 'An unexpected error occurred.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        $tip = \App\Tip::find(request('id'));
        if($tip){
            if($tip->delete()){
                return back()->with('success', 'Tip deleted successfully');
            }
            return back()->with('error', 'An unexpected error occurred.');
        }

        return back()->with('error', 'An unexpected error occurred.');
    }


    public function getTip()
    {
        # code...
        if(is_numeric(request('id'))){
            $tip = \App\Tip::find(request('id'));
            if($tip){
                return array('status' => 1, 'data' => $tip);
            }

            return array('status' => 0, 'data' => []);
        }
    }

    public function history()
    {
        # code...
        $tips = \App\Tip::latest()->paginate(20);
        return view('tips.history', compact('tips'));
    }

    public function addPhoto(Request $request)
    {
        # code...
        #

        $request->validate([
            'tip' => 'required|numeric',
            'photo' => 'required|image'
        ]);
        if(request()->hasFile('photo')){
            $tip = \App\Tip::find($request->tip);
            if($tip){
                $directory = 'uploads/history';
                $fileName = "tips_".$tip->id.'.'.$request->file('photo')->extension();
                $path = $request->file('photo')->storeAs($directory,$fileName);
                $tip->photo = $path;
                $tip->save();
                return back()->with('success', 'Proof added successfully');
            }
        }

        return back()->with('error', 'An unexpected error occurred');


    }

    public function deletePhoto($id)
    {
        # code...
        $tip = \App\Tip::find($id);
        if($tip){
            //$path = getImageLink($tip->photo);
            Storage::delete($tip->photo);

            $tip->removePhoto();
            return back()->with('success', 'Photo removed successfully');
        }
        return back()->with('error', 'An unexpected error occurred');
    }

    public function result($result,$id)
    {
        # code...
        $tip = \App\Tip::find($id);
        if($tip){
            $tip->result = $result;
            $tip->save();
            return back()->with('success', 'Status changed');
        }
        return back()->with('error', 'An unexpected error occurred');
    }

    public function showTipHistory($result,$id)
    {
        # code...
        $tip = \App\Tip::find($id);
        if($tip){
            $tip->show = $result;
            $tip->save();
            return back()->with('success', 'Status changed');
        }
        return back()->with('error', 'An unexpected error occurred');
    }
}
