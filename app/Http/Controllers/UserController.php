<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = \App\Users::paginate(20);
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = \App\Users::find($id);
        //get users credit
        $balance = \App\Credit::where('user_id',$id)->pluck('amount')->sum();
        $payments = \App\Payment::where('user_id', $id)->paginate(10);
        if($user){
            return view('user.show', compact('user', 'balance','payments'));
        }

        return back()->with('error', 'An unexpected error occurred');
    }


    public function block($id)
    {
        //
        $user = \App\Users::find($id);
        if($user){
            $user->block();
            return back()->with('success', 'User has been restricted');
        }

        return back()->with('error', 'An unexpected error occurred');
    }
    public function unblock($id)
    {
        //
        $user = \App\Users::find($id);
        if($user){
            $user->unblock();
            return back()->with('success', 'User has given full access ');


        }

        return back()->with('error', 'An unexpected error occurred');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
