<?php

namespace App\Http\Controllers;

use App\Level;
use App\Library\Paypal\Plan;
use App\Package;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return request()->all();
        $request->validate([
            'name' => 'required',
            'period' => 'required|numeric',
            // 'percent' => 'required|numeric',
            'price' => 'required|numeric',
            'package_id' => 'required|numeric',
            'description' => 'required|',
        ]);
        //create a plan with the


        $level = new \App\Level;
        if(!empty($level->create($request))){
            //create plan
            $package = Package::find($request->package_id);
            if($package->product_id)
                $this->createPlan($package, $level);
            return back()->with('success', 'Membership Level was created successfully');
        }

        return back()->with('error', 'An unexpected error occurred');
    }
    public function storeOnPayPal($id)
    {
        # code...
        $level = Level::find($id);
        if(!$level) return back()->with('error','Unexpected error occurred');
        if(!$level->package->product_id) return back()->with('error', 'Please create this package on paypal first');

        $plan = $this->createPlan($level->package,$level);
        if($plan)
           return back()->with('success', 'Added on Paypal successfully');

        return back()->with('error', 'Error occurred. Please try again');


    }

    public function createPlan($package,$level)
    {
        # code...
        $plan = new Plan;
        $resp = $plan->create($package,$level);
        if($resp->data->id){
            $level->plan_id = $resp->data->id;
            logger((array) $resp->data);
           return $level->save();
        }

        return false;


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'period' => 'required|numeric',

            'price' => 'required|numeric',
            'description' => 'required',

        ]);

        $level =  \App\Level::find($id);
        if($level){
            if($level->edit($request)){
                return back()->with('success', 'Membership Level was updated successfully');
            }
        }


        return back()->with('error', 'An unexpected error occurred');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $level =  \App\Level::find($id);
        if($level){
            if($level->delete()){
                return back()->with('success', 'Membership Level was deleted successfully');
            }
        }
        return back()->with('error', 'An unexpected error occurred');

    }
}
