<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $total_num_of_todays_users = count(\App\Users::whereDate('created_at', \Carbon\Carbon::now()->format('Y-m-d'))->get());
        $num_of_users = count(\App\Users::all());
        $total_amt = \App\Payment::where('status','completed')->pluck('amount')->sum();
        $total_amt_of_purchases_today = \App\Payment::whereDate('created_at', \Carbon\Carbon::now()->format('Y-m-d'))->where('status','completed')->pluck('amount')->sum();
        $users = \App\Users::all();
        $packages = \App\Package::with('levels.memberships')->get();
        $package_arr = [];
        if($packages){
            foreach($packages as $package){
                // $package_arr[$package->name] =
                // dd($package->levels[1]->memberships->count());
                if(count($package->levels)){

                    foreach($package->levels as $level){
                        $package_arr[$package->name] = $level->memberships->count();
                    }
                }
            }
        }
        // $package_arr = (object) $package_arr;
        // dd($package_arr);
        return view('admin.dashboard',
            compact(
                'total_num_of_todays_users',
                'num_of_users',
                'total_amt',
                'total_amt_of_purchases_today',
                'users',
                'package_arr'
                )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
