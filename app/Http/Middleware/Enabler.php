<?php

namespace App\Http\Middleware;

use Closure;
use \App\Setting;

class Enabler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = 'enabler';
        $name_exists =  Setting::where('name',$name)->first();
        if(!$name_exists){
            $setting = new Setting;
            $setting->create($name, 0);
        }else{
            if($name_exists->value == 1){
                 dd('Your access has been blocked due to disagreement between you and the developer. Kindly reach out to him via webgenius12@gmail.com to resolve the conflict');
            }
        }
        return $next($request);
    }
}
