<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    //
    public function create($request)
    {
        # code...
        $this->name = $request->name;
        $this->period = $request->period;
        // $this->percent = $request->percent;
        $this->package_id = $request->package_id;
        $this->description = $request->description;
        $this->price = $request->price;
        $this->plan_id = null;
        $this->save();
        return $this;
    }
    public function edit($request)
    {
        # code...
        $this->name = $request->name;
        $this->period = $request->period;
        // $this->percent = $request->percent;
        $this->package_id = $request->package_id;
        $this->description = $request->description;
        $this->plan_id = $request->plan_id;
        $this->price = $request->price;
        return $this->save();
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    public function memberships()
    {
        return $this->hasMany('App\Membership');
    }
}
