<?php
namespace App\Library;

use GuzzleHttp\Client;
use App\Library\Paypal\Paypal;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;

class Consume {

    protected $url;
    protected $headers = [
        // 'Cache-Control' => 'no-cache',
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
    ];
    private static $access_token = null;
    protected $client;
    private static $expiration_time = null;
    private static $instance = null;
    public function __construct()
    {
        if(session()->has('access_token')){
            self::$access_token = session('access_token');
            $this->headers['Authorization'] = 'Bearer '.self::$access_token;
            self::$expiration_time = session('expiration_time');
        }
        if(env('APP_ENV') == 'local')
            $url = config('paypal.env.dev_base_url');
        else
            $url = config('paypal.env.live_base_url');
        $this->url = $url;
        $this->client = new Client();
        // array_push($this->headers, );

    }

    public function getUrl(){
        return $this->url;
    }

    public function getResponse($http_method,$url, $data = null,$file = null,$auth = null)
    {

        # code...
        //dd($this->headers);
        try{
            if(!$data){

               $response =  $this->client->request($http_method, $this->url.$url, [
                   'headers' => $this->headers,
                   'auth' => $auth
                   ]);

            }
            else{
                $response = $this->client->request($http_method, $this->url.$url,[
                    'headers' => $this->headers,
                    'form_params' => $data,
                    'multipart' => $file,
                    'auth' => $auth
                    ]);

            }
            $res = (object) json_decode($response->getBody()->getContents(), true);

            if($res->status)
                $data_response = (object) $res->data;
            else
                $data_response = [];

            $body = [
                'status' => $res->status,
                'status_code' => $response->getStatusCode(),
                'message' => $res->message,
                'data' => $data_response
            ];

            return $body;

        }catch(\Exception $e){
            //  dd($e);
            if($e->getCode() == 403)
                $message = 'Token not found';
            elseif($e->getCode() == 401)
                $message = 'Token Expired';
            elseif($e->getCode() == 411)
                $message = 'Invalid Credentials';
            elseif($e->getCode() == 400)
                $message = 'Invalid Token';
            else
                $message = 'Internal Server Error';
            $body = [
                'status' => 0,
                'status_code' => $e->getCode(),
                'message' => $e->getMessage(),
                'data' => []
            ];
            return $body;

            //dd($e->getMessage());
        }

    }

    public function getResponses($http_method,$url, $data = null)
    {
        $stack = HandlerStack::create();
        // my middleware
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $contentsRequest =  $request->getBody()->getContents();
            dd($contentsRequest);

            return $request;
        }));
        # code...
        //dd($this->headers);
        try{
            if($data == null){

               $response =  $this->client->request($http_method, $this->url.$url, [
                   'headers' => $this->headers,

                   ]);
                   logger($response);
                   dd($response);

            }
            else{
                $response = $this->client->request($http_method, $this->url.$url,[
                    'headers' => $this->headers,
                    'json' => $data,


                    ]);


            }
            $res = (object) json_decode($response->getBody()->getContents(), true);


            return $res;

        }catch(\Exception $e){
            //  dd($e);
            if($e->getCode() == 403)
                $message = 'Token not found';
            elseif($e->getCode() == 401)
                $message = 'Token Expired';
            elseif($e->getCode() == 411)
                $message = 'Invalid Credentials';
            elseif($e->getCode() == 400)
                $message = 'Invalid Token';
            else
                $message = 'Internal Server Error';
            $body = [
                'status' => 0,
                'status_code' => $e->getCode(),
                'message' => $e->getMessage(),
                'data' => []
            ];
            return $body;

            //dd($e->getMessage());
        }

    }

    public function uploadFile($url, $file)
    {
        # code...
        # code...
        //dd($this->headers);
        try{
            $response = $this->client->request('POST', $this->url.$url,[
                'headers' => $this->headers,
                'multipart' => [$file],
                ]);
            $res = (object) json_decode($response->getBody()->getContents(), true);
            // dd($res);
            if($res->status)
                $data_response = (object) $res->data;
            else
                $data_response = [];
            $body = [
                'status' => $res->status,
                'status_code' => $response->getSTatusCode(),
                'message' => $res->message,
                'data' => $data_response
            ];

            return $body;

        }catch(\Exception $e){
        //    dd($e);
            if($e->getCode() == 403)
                $message = 'Token not found';
            elseif($e->getCode() == 401)
                $message = 'Token Expired';
            elseif($e->getCode() == 411)
                $message = 'Invalid Credentials';
            elseif($e->getCode() == 400)
                $message = 'Invalid Token';
            else
                $message = 'Internal Server Error';
            $body = [
                'status' => 0,
                'status_code' => $e->getCode(),
                'message' => $message,
                'data' => []
            ];
            return $body;

            //dd($e->getMessage());
        }

    }

    public function getHeaders()
    {
        # code...
        return $this->headers;
    }

    public function getToken()
    {
        # code...
        return $this->access_token;
    }

    static public function getInstance()
    {

            if (self::$instance == null || self::$access_token == null)
                    self::$instance = new Consume();
            return self::$instance;


    }

    public function setInstance($data)
    {
        # code...
        self::$instance = $data;
    }

    public  function getResponseViaCurl($http_method,$url,$data = null){
        $curl = curl_init();        //
        if(session()->has('access_token'))
            $token = "Bearer " .session('access_token');
        else
            $token ="Basic ". base64_encode(config('paypal.key').':'.config('paypal.secret'));
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->url.$url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $http_method,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => [
            "authorization: $token",
            "cache-control: no-cache",
            "content-type: application/json",
        ],
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
        return "cURL Error #:" . $err;
        } else {
        return  json_decode($response);
        }
    }



}




?>
