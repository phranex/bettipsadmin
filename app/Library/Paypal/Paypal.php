<?php
namespace App\Library\Paypal;


use GuzzleHttp\Client;

class Paypal
{
    public static $apiContext = null;
    protected static $access_token = null;
    private function __construct()
    {
        self::setAccessToken();
    }

    public static function getApiContext()
    {
        if (self::$apiContext == null)
            self::$apiContext = new Paypal();
        return self::$apiContext;
    }

    public static function getAccessToken(){
        if (self::$access_token == null)
            self::setAccessToken();
        return self::$access_token;
    }

    public static function setAccessToken(){

            $url = "oauth2/token?";
            $auth = [
                config('paypal.key'),
               config('paypal.secret')
            ];
            $data = [
                'grant_type' => 'client_credentials'
            ];
            $client = new Client();
            $response = $client->request('POST', self::getUrl().$url,[
                'headers' => [
                    // 'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],
                'form_params' => $data,
                'auth' => $auth
                ]);
            $response = (object) json_decode($response->getBody()->getContents(), true);;

            return self::$access_token =$response->access_token;

            // self::$expiration_time = $response->expires_in;
            session(['access_token' => $response->access_token]);

            session(['expiration_time' => $response->expires_in]);
    }

    public static function getUrl()
    {
        # code...
        if(env('APP_ENV') == 'local')
            return config('paypal.env.dev_base_url');

        return config('paypal.env.live_base_url');
    }

    public function createPlan()
    {
        # code...

    }


}
