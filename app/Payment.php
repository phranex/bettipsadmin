<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    public function createRecord($user, $ref,$amount, $type,$status =null){
        $this->user_id = $user;

        $this->reference_number = $ref;
        $this->amount = $amount;
        $this->type = $type;
        if(isset($status))
            $this->status = $status;
        return $this->save();
    }

    public function level()
    {
        return $this->belongsTo('\App\Level', 'level_id');
    }

    public function user()
    {
        return $this->belongsTo('\App\Users', 'user_id');
    }
}
