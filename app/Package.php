<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    public function create($name, $description)
    {
        # code...
        $this->name = $name;
        $this->description = $description;
        $this->product_id = null;
        $this->save();
        return $this;
    }
    public function edit($name, $description)
    {
        # code...
        $this->name = $name;
        $this->description = $description;
        return $this->save();
    }
    public function levels()
    {
        return $this->hasMany('App\Level', 'package_id');
    }

    public function remove()
    {
        # code...
        \App\Level::where('package_id', $this->id)->delete();
        return $this->delete();
    }

    public function show()
    {
        # code...
        Package::where('show_packages', 1)->update(['show_packages' => 0]);
        $this->show_packages = 1;
        return $this->save();
    }
}
