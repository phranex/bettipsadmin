<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    public function create($name, $value)
    {
        # code...
        $this->name = $name;
        $this->value = $value;
        return $this->save();
    }

}
