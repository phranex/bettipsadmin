<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('level_id')->nullable();
            $table->integer('package_id')->nullable();
            $table->string('name');
            $table->float('course');
            $table->date('date');
            $table->date('end_date');
            $table->time('time');
            $table->longText('tip');
            $table->integer('confidence');
            $table->boolean('status')->default(1);
            $table->string('amount')->nullable();
            $table->string('photo')->nullable();
            $table->integer('result')->nullable()->default(null);
            $table->boolean('single_purchase_tip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tips');
    }
}
