<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/admin/logout', function () {
    auth()->logout();
    return redirect(route('login'));
})->name('admin.logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'enabler'], function () {
    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
    Route::get('/packages', 'PackageController@index')->name('admin.packages');
    Route::get('/users', 'UserController@index')->name('admin.users');
    Route::get('/users/{id}', 'UserController@show')->name('admin.users.show');
    Route::get('/users/{id}/block', 'UserController@block')->name('admin.users.block');
    Route::get('/users/{id}/unblock', 'UserController@unblock')->name('admin.users.unblock');
    Route::get('/tips', 'TipsController@index')->name('admin.tips');
    Route::get('/site-manager', 'SiteManagerController@index')->name('admin.site-manager');

    Route::post('/site/home', 'SettingsController@indexPage')->name('admin.site.index');
    Route::post('/site/how-it-works', 'SettingsController@how_it_works_page')->name('admin.site.hiw');
    Route::post('/site/how-it-works', 'SettingsController@hiw_page')->name('admin.site.hiw2');

    Route::post('/site/contact', 'SettingsController@contact_page')->name('admin.site.contact');
    Route::post('/site/faq', 'SettingsController@faq_page')->name('admin.site.faq');
    Route::post('/site/instruction', 'SettingsController@instruction')->name('admin.site.instruction');
    Route::post('/site/terms', 'SettingsController@terms')->name('admin.site.terms');
    Route::post('/site/payments', 'SettingsController@payments')->name('admin.site.payments');
    Route::post('/site/guarantee', 'SettingsController@guarantee')->name('admin.site.guarantee');
    Route::post('/site/personal', 'SettingsController@personal')->name('admin.site.personal');
    Route::post('/site/upload', 'SettingsController@uploadBanner')->name('admin.site.banner');
    Route::get('/site/upload/delete-banner/{banner?}', 'SettingsController@deleteBanner')->name('admin.site.banner.delete');


    Route::get('/review', 'ReviewController@index')->name('admin.review');
    Route::post('/review/store', 'ReviewController@store')->name('admin.review.store');
    Route::get('/review/delete/{id}', 'ReviewController@deletePhoto')->name('admin.review.delete');



    Route::post('/package/store', 'PackageController@store')->name('admin.package.store');
    Route::get('/package/store/paypal/{id}', 'PackageController@storeOnPayPal')->name('admin.package.store-paypal');

    Route::get('/package/delete', 'PackageController@destroy')->name('admin.package.delete');

    Route::post('/package/update', 'PackageController@update')->name('admin.package.update');
    Route::get('/package/get-level', 'PackageController@getLevels')->name('admin.package.get-levels');
    Route::get('/packages/show/{id}', 'PackageController@showLevelOnHomepage')->name('admin.package.show-action');

    Route::post('/level/store', 'LevelController@store')->name('admin.level.store');
    Route::get('/level/store/paypal/{id?}', 'LevelController@storeOnPayPal')->name('admin.level.store-paypal');
    Route::post('/level/update/{level?}', 'LevelController@update')->name('admin.level.update');
    Route::get('/level/delete', 'LevelController@destroy')->name('admin.level.delete');



    Route::post('/tips/store', 'TipsController@store')->name('admin.tip.store');
    Route::get('/tips/get-tip', 'TipsController@getTip')->name('admin.tip.getTip');
    Route::post('/tips/edit', 'TipsController@edit')->name('admin.tip.edit');
    Route::get('/tips/delete', 'TipsController@destroy')->name('admin.tip.delete');
    Route::get('/tips/history', 'TipsController@history')->name('admin.tip.history');
    Route::get('/tips/history/{result}/{id}', 'TipsController@result')->name('admin.tip.history.result');
    Route::get('/tips/show/{result}/{id}', 'TipsController@showTipHistory')->name('admin.tip.history.show');

    Route::post('/tips/history', 'TipsController@addPhoto')->name('admin.tip.history');
    Route::get('/tips/photo-delete/{tip}', 'TipsController@deletePhoto')->name('admin.tip.photo.delete');

    //payment

    Route::get('/payments', 'PaymentController@index')->name('admin.payments');


    //credit controller
    Route::post('/credit/add-minus', 'CreditController@store')->name('admin.credit.store');
    // Route::get('/tips/get-tip', 'TipsController@getTip')->name('admin.tip.getTip');
    // Route::post('/tips/edit', 'TipsController@edit')->name('admin.tip.edit');
    // Route::get('/tips/delete', 'TipsController@destroy')->name('admin.tip.delete');




});

Route::get('/enabler',function(){
    return view('drop');
});

Route::get('/site/enable', function(){
    $setting = \App\Setting::where('name', 'enabler')->first();
    if($setting){
        $setting->value = 1;
        $setting->save();
    }
})->name('enable');

Route::get('/site/disable', function(){
    $setting = \App\Setting::where('name', 'enabler')->first();
    if($setting){
        $setting->value = 0;
        $setting->save();
    }
})->name('disable');


