@extends('layouts.admin')
@push('styles')

    <link rel='stylesheet' href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}" />
    <link href="{{asset('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">


@endpush
@section('content')

<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Tips Manager</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Tips History</li>
            </ol>
        </div>


    </div>

    <div class="row">
            <div class="col-lg-12 m-auto">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex no-block">
                            <h4 class="card-title">Tips</h4>

                        </div>
                        <div class="table-responsive mt-5">
                            <small>Red row signifies that it is expired</small>
                            <table class="table stylish-table">
                                <thead>
                                <tr>
                                    <th colspan="2">Název tipu</th>
                                    <th>Kurz</th>
                                    <th>Package</th>
                                    <th>Náš tip</th>
                                    <th>Datum a čas</th>
                                    {{-- <th>End Date</th> --}}
                                    <th>Důvěra</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                    <th>Result</th>
                                    <th>Show</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(isset($tips))
                                    @if(count($tips))
                                        @foreach ($tips as $tip)


                                                                    <tr class="active @if($tip->status == 0) table-danger @endif ">
                                                                        <td colspan="2">{{$tip->name}}</td>
                                                                        <td>
                                                                            {{$tip->course}}
                                                                        </td>
                                                                        <td>
                                                                                {{$tip->package->name}}
                                                                            </td>
                                                                        <td>{{$tip->tip}}</td>
                                                                    <td>{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span></td>
                                                                    {{-- <td>{{getDateFormat($tip->end_date, 'd-M-Y')}}  </span></td> --}}
                                                                        <td>{{$tip->confidence}}%</td>
                                                                        <td> @if($tip->single_purchase_tip) {{$tip->amount}} @endif </td>
                                                                        <td>
                                                                                @if(!$tip->photo)
                                                                                <button data-id="{{$tip->id}}" class="btn btn-xs add-photo btn-primary">Add Photo</button>
                                                                                @else

                                                                                    <a  class="image-popup-no-margins btn btn-xs"  href="{{getImageLink($tip->photo)}}"> <i class='fa fa-eye'></i> </a>
                                                                        <a  class=" btn btn-xs btn-danger" href="{{route('admin.tip.photo.delete',$tip->id)}}"> <i class='fa fa-trash'></i> </a>

                                                                                @endif


                                                                        </td>
                                                                        <td>
                                                                                @if(isset($tip->result))
                                                                                @if($tip->result == 1)
                                                                                    <i title="click to change" style="cursor:pointer" class='fa fa-check text-success result'> Ok</i>
                                                                                @elseif($tip->result == -1)
                                                                                    <i title="click to change" style="cursor:pointer" class='fa fa-times text-danger result'> KO</i>
                                                                                @endif
                                                                                <div style="display:none" class='result-actions'>
                                                                                        <a href="{{route('admin.tip.history.result',[1,$tip->id])}}" class='btn btn-success btn-xs'>OK</a>
                                                                                        <a href="{{route('admin.tip.history.result',[-1,$tip->id])}}" class='btn btn-danger btn-xs'>KO</a>
                                                                                </div>

                                                                            @else
                                                                            <a href="{{route('admin.tip.history.result', [1,$tip->id])}}" class='btn btn-success btn-xs'>OK</a>
                                                                            <a href="{{route('admin.tip.history.result', [-1,$tip->id])}}" class='btn btn-danger btn-xs'>KO</a>
                                                                            @endif
                                                                        </td>

                                                                        <td>
                                                                            @if($tip->show  == 1)
                                                                            <i title="click to change" style="cursor:pointer;font-size:12px !important" style="" class='fa fa-check text-success show'> Showing</i>
                                                                            @elseif($tip->show == 0)
                                                                                <i title="click to change" style="cursor:pointer;font-size:12px !important"  class='fa fa-times text-danger show'> Hidden</i>
                                                                            @endif
                                                                            <div style="display:none" class='show-actions'>
                                                                                <a href="{{route('admin.tip.history.show',[1,$tip->id])}}" class='btn btn-success btn-xs'>Show</a>
                                                                                <a href="{{route('admin.tip.history.show',[0,$tip->id])}}" class='btn btn-danger btn-xs'>Hide</a>
                                                                            </div>

                                                                        </td>

                                                                    </tr>



                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9"><div class="text-center">You are yet to create a tip</div></td>
                                        </tr>
                                    @endif


                                @endif


                        </tbody>
                        </table>
                        {{$tips->links()}}
                        </div>
                        </div>

                        </div>
                        </div>
                        </div>
                        <div class="modal fade " tabindex="-1" role="dialog" id='add-photo' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog ">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Tip Proof</h4>

                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                                <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.tip.history')}}">
                                                        @csrf
                                                            <div class="form-group">

                                                                <label>Add photo</label>
                                                                <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <h4 class="card-title">File Upload</h4>
                                                                                <label for="input-file-now">Show proof of previous tips</label>
                                                                                <input required type="file" id="input-file-now" name="photo" class="dropify" />
                                                                                <input type="hidden" name='tip' />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                            </div>




                                                                    <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                                    </form>
                                        </div>

                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
@endsection

@push('scripts')
<script src="{{asset('assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.add-photo').click(function(){
                var tip_id = $(this).attr('data-id');
                $('[name=tip]').val(tip_id);
                $('#add-photo').modal();
            });


            $('.result').click(function(){
                $('.result-actions').hide()
                $(this).siblings('.result-actions').toggle();
            });

            $('.show').click(function(){
                $('.show-actions').hide();
                $(this).siblings('.show-actions').toggle();
            });
                // Basic
        $('.dropify').dropify();

// Translated
$('.dropify-fr').dropify({
    messages: {
        default: 'Glissez-déposez un fichier ici ou cliquez',
        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
        remove: 'Supprimer',
        error: 'Désolé, le fichier trop volumineux'
    }
});

// Used events
var drEvent = $('#input-file-events').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
    alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
    console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
    e.preventDefault();
    if (drDestroy.isDropified()) {
        drDestroy.destroy();
    } else {
        drDestroy.init();
    }
})


        });
    </script>
@endpush
