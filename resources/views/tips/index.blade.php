@extends('layouts.admin')
@section('content')


<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Tips Manager</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Tips Manager</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">

                <button class="btn float-right btn-success create-tip"><i class="mdi mdi-plus-circle"></i> Create Tips</button>
                <a class="btn float-right btn-sm btn-link " href='{{route('admin.tip.history')}}'>Tips History</a>
            </div>

    </div>

    @if(isset($grouped_tips))
    @if(count($grouped_tips))
        @foreach ($grouped_tips as $tips)
        <div class="row">
                <div class="col-lg-10 m-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex no-block">
                                <h4 class="card-title">{{$tips[0]->package->name}}</h4>

                            </div>
                            <div class="table-responsive mt-5">
                                <table class="table stylish-table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Název tipu</th>
                                        <th>Kurz</th>
                                        <th>Náš tip</th>
                                        <th>Datum a čas</th>
                                        <th>End Date</th>
                                        <th>Důvěra</th>
                                       <th>Amount</th>
                                        <th>Actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($tips as $tip)

                                    <tr class="active">
                                        <td colspan="2">{{$tip->name}}</td>
                                        <td>
                                            {{$tip->course}}
                                        </td>
                                        <td>{{$tip->tip}}</td>
                                    <td>{{getDateFormat($tip->date, 'd-M-Y')}}  <span style='font-size:12px'>{{getDateFormat($tip->time, 'h:i A')}}</span></td>
                                    <td>{{getDateFormat($tip->end_date, 'd-M-Y')}}  </span></td>
                                        <td>{{$tip->confidence}}%</td>
                                        <td>@if($tip->single_purchase_tip) {{$tip->amount}} @else  @endif </td>
                                        <td>
                                                <button data-id="{{$tip->id}}" class="btn btn-xs edit btn-primary">Edit</button>
                                                    <button data-id="{{$tip->id}}" class="btn btn-xs delete btn-danger">Delete</button>
                                                </td>

                                    </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>


        @endforeach
    @else
        <tr>
            <td colspan="9"><div class="text-center">You are yet to create a tip for today. Please view your history for previous tips</div></td>
        </tr>
    @endif
    <div class="alert "> {{$tips_->links()}}</div>

@endif



        <div class="modal fade " tabindex="-1" role="dialog" id='tip-create' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Tip Creation</h4>

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                                <form id='tip-form' method='post' action="{{route('admin.tip.store')}}">
                                        @csrf
                                            <div class="form-group">
                                                {{-- <label>Membership Level</label>
                                                <select class="form-control" name='level' required>
                                                    <option value=''>Select Membership</option>
                                                    @if(isset($levels))
                                                        @if(count($levels))
                                                            @foreach ($levels as $level)
                                                                <option @if(old('level') == $level->id) selected @endif value="{{$level->id}}">{{$level->name}}</option>
                                                            @endforeach
                                                        @endif

                                                    @endif
                                                </select> --}}
                                                <label>Package</label>
                                                <select class="form-control" name='package' required>
                                                    <option value=''>Select Package</option>
                                                    @if(isset($packages))
                                                        @if(count($packages))
                                                            @foreach ($packages as $level)
                                                                <option @if(old('package') == $level->id) selected @endif value="{{$level->id}}">{{$level->name}}</option>
                                                            @endforeach
                                                        @endif

                                                    @endif
                                                </select>
                                            </div>

                                            <div class="form-group row">
                                                <div class='col'>
                                                        <label>Name</label>
                                                <input type='text' value='{{old('name')}}' name="name" class='form-control' required />
                                                </div>

                                                <div class='col'>
                                                        <label>Course</label>
                                                        <input type='number' name="course" step='0.01' class='form-control' required />
                                                </div>


                                            </div>

                                            <div class="form-group">
                                                    <div class='col'>
                                                            <label>Tip</label>
                                                            <textarea  name="tip" rows="3" class='form-control' required ></textarea>
                                                    </div>
                                            </div>

                                            <div class="form-group row">
                                                    <div class='col'>
                                                            <label>Date</label>
                                                    <input type='date' required name='date'  class='form-control' required />
                                                    </div>

                                                    <div class='col'>
                                                            <label>Time</label>
                                                            <input type='time' name='time' value='{{getDateFormat(\Carbon\Carbon::now(), 'H:m')}}'  class='form-control' required />
                                                    </div>
                                                    <div class='col'>
                                                            <label>End Date</label>
                                                            <input type='date' name='end_date' value='{{getDateFormat(\Carbon\Carbon::now()->addDay(1),'Y-m-d')}}'  class='form-control' required />
                                                    </div>
                                                    <div class='col'>
                                                            <label>confidence</label>
                                                            <input type='number' name='confidence'  class='form-control' required />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                Single Purchase
                                                <input type="checkbox" name='single' />
                                                <div style="display:none" id='amt'>
                                                <label>Amount</label>
                                                <input  class='form-control col-6' type="number"  name='amount' />
                                                </div>
                                            </div>



                                                    <button type="button" class="btn btn-primary submit">Submit</button>

                                    </form>
                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>



@endsection
@push('scripts')

    <Script>
            $('.create-tip').click(function(){
                $('[name=name]').val('');
                $('[name=course]').val('');
                $('[name=confidence]').val('');
                $('[name=package]').val('');
                $('[name=tip]').val('');
                $('[name=amount]').val('');
                $('#amt input').prop('required', false);
                $('[name=single]').prop('checked', false);
                $('#tip-form').attr('action', '{{route('admin.tip.store')}}');
                $('#tip-create').modal();
            });

            $('[name=single]').click(function(){
                if($(this).is(':checked')){
                    $('#amt').show();
                    $('#amt input').attr('required', true);

                }else{
                    $('#amt input').prop('required', false);
                    $('#amt').hide();
                }
            });

            $('.edit').click(function(){

                var id = $(this).attr('data-id');
                getTip(id);
            });

            $('.delete').click(function(){
                var id = $(this).attr('data-id');
               var answer = confirm("Are you sure? If yes, click OK to continue");
               if(answer){
                   window.location.href = "{{route('admin.tip.delete')}}?id="+id;
               }
            });

            $('.submit').click(function(){
                var date = new Date($('[name=date]').val());
                var end_date = new Date($('[name=end_date]').val());

                if(end_date > date){
                    $(this).attr('type','submit');
                    //$('#tip-form').submit();
                }
                else{
                    $.toast({
                        heading: 'Error',
                        text: 'End Date must be greater than date',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500

                    });
                }
            });


            function getTip(id){
                var url = '{{route('admin.tip.getTip')}}' + '?id=' +id;
                $.ajax({
                    url: url,
                    success: function(result){
                        if(result.status){
                            var html = '';
                            console.log(result);
                            if(result.status){
                                $('[name=name]').val(result.data.name);
                                $('[name=course]').val(result.data.course);
                                $('[name=confidence]').val(result.data.confidence);
                                $('[name=date]').val(result.data.date);
                                $('[name=time]').val(result.data.time);
                                $('[name=package]').val(result.data.package_id);
                                $('[name=tip]').val(result.data.tip);
                                console.log(result.data.time);
                                if(result.data.single_purchase_tip){
                                    $('[name=single]').prop('checked', true);
                                    $('[name=amount]').val(result.data.amount);
                                    $('#amt input').attr('required', true);
                                }else{
                                    $('[name=single]').prop('checked', false);
                                    $('#amt input').attr('required', false);
                                }
                                $('#tip-form').attr('action', '{{route('admin.tip.edit')}}?id='+id)
                                $('#tip-create').modal();

                            }else{
                                $.toast({
                                    heading: 'Error',
                                    text: 'An unexpected error occurred. Please try again',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500

                                });
                            }
                            $('#level-holder').html(html);

                        }
                    },
                    error: function(){
                        $.toast({
                            heading: 'Error',
                            text: 'An unexpected error occurred. Please try again',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500

                        });
                    }
                });
            }


    </script>
@endpush
