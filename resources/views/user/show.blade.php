@extends('layouts.admin')
@section('content')

<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Můj profil</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Domů</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.users')}}"> Users</a></li>
                <li class="breadcrumb-item active"> Profil</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <center class="mt-4">
                        <h4 class="card-title mt-2">{{$user->name}}</h4>@if($user->blocked)
                        <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-danger'><i class='fa fa-lock-o'></i> Unblock</a>
                        @else
                            <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-danger'><i class='fa fa-lock'></i> Block</a>
                        @endif
                        <button class='btn btn-xs btn-primary manage-credit' ><i class='fa fa-lock'></i> Manage Credit</button>
                        {{-- <h6 class="card-subtitle">Accoubts Manager Amix corp</h6> --}}
                        {{-- <div class="row text-center justify-content-md-center">
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                        </div> --}}
                    </center>
                </div>
                <div>
                    <hr> </div>
                <div class="card-body"> <small class="text-muted">Emailová adresa </small>
                    <h6>{{$user->email}}</h6>
                    {{-- <small class="text-muted p-t-30 db">Phone</small> --}}
                    {{-- <h6>+91 654 784 547</h6> <small class="text-muted p-t-30 db">Address</small>
                    <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6> --}}

                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs profile-tab" role="tablist">

                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Transaction History</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#password" role="tab">Testimonial</a> </li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content">

                    <div class="tab-pane active" id="settings" role="tabpanel">
                        <div class="card-body">
                                <div class="table-responsive">
                                        <table class="table table-hover no-wrap">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Název transakce</th>
                                                    <th>Datum</th>
                                                    <th>Cena</th>
                                                    <th>Reference Number</th>
                                                    <th>Stav</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($payments))
                                                    @if(count($payments))
                                                        @foreach($payments as $payment)

                                                            <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{ucwords($payment->type)}}</td>
                                                                <td>{{getDateFormat($payment->created_at, 'd-M-Y')}}</td>
                                                            <td>{{$payment->amount}}</td>
                                                            <td>{{$payment->reference_number}}</td>
                                                                <td>
                                                                    @if($payment->status == 'completed')
                                                                    <i class='fa fa-check'></i> Verified
                                                                    @elseif($payment->status == 'pending')
                                                                    <i class='fa fa-hourglass '></i> {{ucwords($payment->status)}}
                                                                    @else
                                                                    <i class='fa fa-times '></i> {{ucwords($payment->status)}}
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        @else
                                                        <tr>
                                                            <Td colspan='6'>
                                                                <div class='alert alert-default text-center'>No payments found</div>
                                                            </td>
                                                        </tr>

                                                    @endif

                                                @endif

                                                {{$payments->links()}}

                                            </tbody>
                                        </table>
                                    </div>
                        </div>
                    </div>

                    <div class="tab-pane " id="password" role="tabpanel">
                        <div class="card-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->


    <div class="modal fade " tabindex="-1" role="dialog" id='credit' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Manage Credit for <strong>{{$user->name}}</strong> </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <p>Total Credit: <span class='text-bold'>{{$balance}}<span> </p>
                        <form method='post' action="{{route('admin.credit.store')}}">
                                @csrf
                                    <small>You can credit and debit this user's account here. To debit 10, enter <strong>-10</strong>. To add 10, enter <strong>10</strong></small>
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input required name='amount' value="" type="number" class="form-control" placeholder="">
                                    </div>
                                <input type='hidden' name='user_id' value='{{$user->id}}' />


                                    <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div> --}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@push('scripts')

    <Script>

            $('.manage-credit').click(function(){
                $('#credit').modal();
            });



    </script>
@endpush
