@extends('layouts.admin')
@section('content')


<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Users</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
        </div>


    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <h4 class="card-title">Users</h4>

                    </div>
                    <div class="table-responsive mt-5">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Membership Level</th>
                                    {{-- <th>Days Left</th> --}}
                                    <th>Registered</th>
                                    <th>Activated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($users))
                                    @if(count($users))
                                        @foreach($users as $user)
                                        <tr>
                                        <td style="width:50px;"><span class="round">{{strtoupper($user->name[0])}}</span></td>
                                            <td>
                                                <h6>{{ucwords($user->name)}}</h6>
                                                {{-- <small class="text-muted">Web Designer</small> --}}
                                            </td>
                                            <td>
                                                @if(count($user->memberships))
                                                    {{getLevel($user->memberships)}}
                                                @else
                                                    No level bought

                                                @endif
                                            </td>
                                            {{-- <td><span class="label label-light-success">21 days</span></td> --}}
                                            <td>{{getDateFormat($user->created_at, 'd-m-Y')}}</td>
                                            <td>
                                                @if($user->verified)
                                                    Yes
                                                @else
                                                    No
                                                @endif
                                            </td>
                                            <td>
                                            <a href='{{route('admin.users.show', $user->id)}}' class='btn btn-xs btn-info'>View</a>
                                            @if($user->blocked)
                                            <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-danger'>unblock</a>
                                            @else
                                                <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-danger'>Block</a>
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif

                                @endif


                            </tbody>
                        </table>
                        {{$users->links()}}
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id='learn-more' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Extra Large modal</h4>
                        <div class="col-md-6 col-4 align-self-center">
                                <button class="btn float-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Add Membership Level</button>

                            </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <div class="table-responsive">
                                    <table class="table table-striped no-wrap">
                                        <thead>
                                            <tr>
                                                <th>Task</th>
                                                <th>Progress</th>
                                                <th>Deadline</th>
                                                <th class="text-nowrap">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Lunar probe project</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-danger" style="width: 35% ;height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>May 15, 2015</td>
                                                <td class="text-nowrap">
                                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse mr-2"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Dream successful plan</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-warning" style="width: 50%; height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>July 1, 2015</td>
                                                <td class="text-nowrap">
                                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse mr-2"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Office automatization</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-success" style="width: 100%; height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>Apr 12, 2015</td>
                                                <td class="text-nowrap">
                                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse mr-2"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>The sun climbing plan</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-primary" style="width: 70%; height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>Aug 9, 2015</td>
                                                <td class="text-nowrap">
                                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse mr-2"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Open strategy</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-info" style="width: 85%; height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>Apr 2, 2015</td>
                                                <td class="text-nowrap">
                                                    <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse mr-2"></i> </a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tantas earum numeris</td>
                                                <td>
                                                    <div class="progress progress-xs margin-vertical-10 ">
                                                        <div class="progress-bar bg-inverse" style="width: 50%; height:6px;"></div>
                                                    </div>
                                                </td>
                                                <td>July 11, 2015</td>
                                                <td class="text-nowrap">

                                                    <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> Upgrade</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


@endsection
@push('scripts')

    <Script>
            $('.learn-more').click(function(){
                $('#learn-more').modal();
            });
    </script>
@endpush
