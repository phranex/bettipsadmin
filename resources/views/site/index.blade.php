@extends('layouts.admin')

@push('styles')
<link rel='stylesheet' href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}" />
<link href="{{asset('assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet">


<style>
        .ck-editor__editable {
            min-height: 400px;
        }

        .banner-img-holder{
            width: 200px;
            display: inline-block;
        }

</style>

@endpush
@section('content')

<div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body pb-0">
                <h4 class="card-title">Manage Your Site</h4>
                {{-- <h6 class="card-subtitle">Use default tab with class <code>customtab</code></h6> </div> --}}
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Home</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#how-it-works" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">How It works</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#faq" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">FAQ</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#contact" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Contact Us</span></a> </li>
                {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#others" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Instructions</span></a> </li> --}}
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#banner" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Banner</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#payments" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Automatic Payments</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#terms" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Terms</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#guarantee" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">30 Day Guarantee</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#personal" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Personal Data</span></a> </li>


                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home2" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">

                                            <form method='post' action="{{route('admin.site.index')}}" class="form-horizontal mt-4">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label>Title on Banner <span class="help"></span></label>
                                                    <input required name='banner-title' type="text" class="form-control" value="@if(old('banner-title'))  {{old('banner-title')}} @else {{@$contents['index']['banner-title']}} @endif">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="example-email">About Us Subtitle<span class="help"></label>
                                                        <input required name='about-us-subtitle' type="text" value="@if(old('about-us-subtitle'))  {{old('about-us-subtitle')}} @else {{@$contents['index']['about-us-subtitle']}} @endif" class="form-control" placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                            <label>About Us description</label>
                                                            <textarea required name="about-us-description"  class="form-control" rows="5">@if(old('about-us-description'))  {{old('about-us-description')}} @else {{@$contents['index']['about-us-description']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                            <label>Membership plan subtitle</label>
                                                        <input  required name="membership-plan-subtitle" type="text" value="@if(old('membership-plan-subtitle'))  {{old('membership-plan-subtitle')}} @else {{@$contents['index']['membership-plan-subtitle']}} @endif" class="form-control" placeholder="">
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Why use us subtitle</label>
                                                        <input required name="why-use-us-subtitle" type="text" value="@if(old('about-us-subtitle'))  {{old('about-us-subtitle')}} @else {{@$contents['index']['about-us-subtitle']}} @endif" class="form-control" placeholder="placeholder">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Why use us: reason 1 Title</label>
                                                        <input class="form-control" name='why-use-us-title-1' value="@if(old('why-use-us-title-1'))  {{old('why-use-us-title-1')}} @else {{@$contents['index']['why-use-us-title-1']}} @endif" />
                                                        <label>Why use us: reason 1</label>
                                                        <textarea required required name="why-use-us-subtitle-reason1"  class="form-control" rows="5">@if(old('why-use-us-subtitle-reason1'))  {{old('why-use-us-subtitle-reason1')}} @else {{@$contents['index']['why-use-us-subtitle-reason1']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                            <label>Why use us: reason 2 Title</label>
                                                            <input class="form-control" name='why-use-us-title-2' value="@if(old('why-use-us-title-2'))  {{old('why-use-us-title-2')}} @else {{@$contents['index']['why-use-us-title-2']}} @endif" />
                                                            <label>Why use us: reason 2</label>
                                                            <textarea required required name="why-use-us-subtitle-reason2" class="form-control" rows="5">@if(old('why-use-us-subtitle-reason2'))  {{old('why-use-us-subtitle-reason2')}} @else {{@$contents['index']['why-use-us-subtitle-reason2']}} @endif</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                            <label>Why use us: reason 3 Title</label>
                                                            <input class="form-control" name='why-use-us-title-3' value="@if(old('why-use-us-title-3'))  {{old('why-use-us-title-3')}} @else {{@$contents['index']['why-use-us-title-3']}} @endif" />
                                                            <label>Why use us: reason 3</label>
                                                            <textarea required name="why-use-us-subtitle-reason3"  class="form-control" rows="5">@if(old('why-use-us-subtitle-reason3'))  {{old('why-use-us-subtitle-reason3')}} @else {{@$contents['index']['why-use-us-subtitle-reason3']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                            <label>Footer Description</label>
                                                            <textarea required name='footer-description'  class="form-control" rows="5">@if(old('footer-description'))  {{old('footer-description')}} @else {{@$contents['index']['footer-description']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>How it Works Description</label>
                                                        <textarea required name='hiw-description'  class="form-control" rows="5">@if(old('hiw-description'))  {{old('hiw-description')}} @else {{@$contents['index']['hiw-description']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Contact Description</label>
                                                        <textarea required name='contact-description'  class="form-control" rows="5">@if(old('contact-description'))  {{old('contact-description')}} @else {{@$contents['index']['contact-description']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Review Description</label>
                                                        <textarea required name='review-description'  class="form-control" rows="5">@if(old('review-description'))  {{old('review-description')}} @else {{@$contents['index']['review-description']}} @endif</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                            <label>Facebook Link</label>
                                                            <input required name="facebook" type="text" value="@if(old('facebook'))  {{old('facebook')}} @else {{@$contents['index']['facebook']}} @endif" class="form-control" placeholder="placeholder">
                                                        </div>

                                                    <div class="form-group">
                                                            <label>Twitter Link</label>
                                                            <input required name="twitter" type="text" value="@if(old('twitter'))  {{old('twitter')}} @else {{@$contents['index']['twitter']}} @endif" class="form-control" placeholder="placeholder">
                                                        </div>
                                                    <div class="form-group">
                                                            <label>Instagram Link</label>
                                                            <input required name="instagram" type="text" value="@if(old('instagram'))  {{old('instagram')}} @else {{@$contents['index']['instagram']}} @endif" class="form-control" placeholder="placeholder">
                                                        </div>
                                                    <div class="custom-control custom-switch">
                                                            <input type="checkbox" name="show-package" @if(!empty($contents['index']['show-package'])) checked @endif class="custom-control-input" id="customSwitch1">
                                                            <label class="custom-control-label" for="customSwitch1"> Show package on HomePage</label>
                                                        </div>
                                                    <div class="form-group m-t-15">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    {{-- <div class="tab-pane" id="how-it-works" role="tabpanel">

                            <div class="p-3">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card card-body">

                                                    <form method='post' action="{{route('admin.site.hiw')}}" class="form-horizontal mt-4">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="example-email">How it works Subtitle</label>
                                                        <input required type="text"  name="how-it-works-subtitle" value="@if(old('how-it-works-subtitle'))  {{old('how-it-works-subtitle')}} @else {{@$contents['hiw']['how-it-works-subtitle']}} @endif" class="form-control" placeholder="">
                                                        </div>
                                                        <div class="form-group">
                                                                <label>How it works description</label>
                                                                <textarea required name="how-it-works-description" class="form-control" rows="5">@if(old('how-it-works-subtitle'))  {{old('how-it-works-description')}} @else {{@$contents['hiw']['how-it-works-description']}} @endif</textarea>
                                                        </div>


                                                        <div class="form-group">
                                                            <label>How it works: step 1</label>
                                                            <textarea required name="how-it-works-step1" class="form-control" rows="5">@if(old('how-it-works-subtitle'))  {{old('how-it-works-step1')}} @else {{@$contents['hiw']['how-it-works-step1']}} @endif</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                                <label>How it works: step 2</label>
                                                                <textarea required name="how-it-works-step2" class="form-control" rows="5">@if(old('how-it-works-subtitle'))  {{old('how-it-works-step2')}} @else {{@$contents['hiw']['how-it-works-step2']}} @endif</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                                <label>How it works: reason 3</label>
                                                                <textarea required name="how-it-works-step3" class="form-control" rows="5">@if(old('how-it-works-subtitle'))  {{old('how-it-works-step3')}} @else {{@$contents['hiw']['how-it-works-step3']}} @endif</textarea>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Submit</button>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                    </div> --}}
                    <div class="tab-pane p-3" id="how-it-works" role="tabpanel">

                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">

                                                <form method='post' action="{{route('admin.site.hiw2')}}">
                                                        @csrf
                                                        @if(isset($contents['how']))
                                                            @if(count($contents['how']))
                                                            <div class="form-group add-more2">
                                                                @foreach ($contents['how'] as $key => $value)


                                                                        <label>Step {{$loop->iteration}}</label>
                                                            <textarea required name='step{{$loop->iteration}}'  class="form-control" rows="2">{{$key}}</textarea>


                                                                        <label>Answer</label>
                                                                        <textarea required name='answer{{$loop->iteration}}' class="form-control" rows="5">{{$value}}</textarea>

                                                                @endforeach
                                                            </div>
                                                            @endif
                                                        @else
                                                        <div class="form-group add-more2">

                                                                <label>Step</label>
                                                                <textarea required name='step1' class="form-control" rows="2"></textarea>


                                                                <label>Answer</label>
                                                                <textarea required name='answer1' class="form-control" rows="5"></textarea>
                                                        </div>

                                                        @endif


                                                <button type="button" class="btn btn-xs add-step waves-effect btn-outline-info waves-light">Add Step</button>

                                                        <button type="submit" class="btn btn-primary pull-right">Submit</button>

                                                </form>
                                                <div class="text-right">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="tab-pane p-3" id="faq" role="tabpanel">

                            <div class="p-3">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card card-body">

                                                    <form method='post' action="{{route('admin.site.faq')}}">
                                                            @csrf
                                                            @if(isset($contents['faq']))
                                                                @if(count($contents['faq']))
                                                                <div class="form-group add-more">
                                                                    @foreach ($contents['faq'] as $key => $value)


                                                                            <label>Question</label>
                                                                <textarea required name='question{{$loop->iteration}}'  class="form-control" rows="2">{{$key}}</textarea>


                                                                            <label>Answer</label>
                                                                            <textarea required name='answer{{$loop->iteration}}' class="form-control" rows="5">{{$value}}</textarea>

                                                                    @endforeach
                                                                </div>
                                                                @endif
                                                            @else
                                                            <div class="form-group add-more">

                                                                    <label>Question</label>
                                                                    <textarea required name='question1' class="form-control" rows="2"></textarea>


                                                                    <label>Answer</label>
                                                                    <textarea required name='answer1' class="form-control" rows="5"></textarea>
                                                            </div>

                                                            @endif


                                                    <button type="button" class="btn btn-xs add-faq waves-effect btn-outline-info waves-light">Add FAQ</button>

                                                            <button type="submit" class="btn btn-primary pull-right">Submit</button>

                                                    </form>
                                                    <div class="text-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                    </div>
                    <div class="tab-pane p-3" id="contact" role="tabpanel">
                            <div class="p-3">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card card-body">
                            <form method='post' action="{{route('admin.site.contact')}}">
                                @csrf
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input required name='address' value="@if(old('address'))  {{old('address')}} @else {{@$contents['contact']['address']}} @endif" type="text" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                            <label>Phone Number</label>
                                            <input required name="phone-number" type="text" value="@if(old('phone-number'))  {{old('phone-number')}} @else {{@$contents['contact']['phone-number']}} @endif"  class="form-control" placeholder="">

                                        </div>
                                        <div class="form-group">
                                                <label>Email</label>
                                                <input required name='email' type="email" class="form-control" value="@if(old('email'))  {{old('email')}} @else {{@$contents['contact']['email']}} @endif" placeholder="">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    {{-- <div class="tab-pane p-3" id="others" role="tabpanel">
                            <div class="p-3">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card card-body">
                                                        <form action='{{route('admin.site.instruction')}}' method="post">
                                                            @csrf
                                                                <textarea rows="5"  id="editor" name="editordata"></textarea>

                                                                <div class='m-t-20 text-right'>
                                                                    <button type="submit" class='btn btn-md btn-primary'>Save</button>
                                                                </div>
                                                        </form>

                                                </div>
                                            </div>
                                    </div>
                            </div>
                    </div> --}}
                    <div class="tab-pane p-3" id="terms" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">
                                                    <form action='{{route('admin.site.terms')}}' method="post">
                                                        @csrf
                                                            <textarea rows="5"  id="terms_" name="editordata"></textarea>

                                                            <div class='m-t-20 text-right'>
                                                                <button type="submit" class='btn btn-md btn-primary'>Save</button>
                                                            </div>
                                                    </form>

                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane p-3" id="payments" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">
                                                    <form action='{{route('admin.site.payments')}}' method="post">
                                                        @csrf
                                                            <textarea rows="5"  id="payments_" name="editordata"></textarea>

                                                            <div class='m-t-20 text-right'>
                                                                <button type="submit" class='btn btn-md btn-primary'>Save</button>
                                                            </div>
                                                    </form>

                                            </div>
                                        </div>
                                </div>

                        </div>
                    </div>
                    <div class="tab-pane p-3" id="guarantee" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">
                                                    <form action='{{route('admin.site.guarantee')}}' method="post">
                                                        @csrf
                                                            <textarea rows="5"  id="guarantee_" name="editordata"></textarea>

                                                            <div class='m-t-20 text-right'>
                                                                <button type="submit" class='btn btn-md btn-primary'>Save</button>
                                                            </div>
                                                    </form>

                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane p-3" id="personal" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-body">
                                                    <form action='{{route('admin.site.personal')}}' method="post">
                                                        @csrf
                                                            <textarea rows="5"  id="personal_" name="editordata"></textarea>

                                                            <div class='m-t-20 text-right'>
                                                                <button type="submit" class='btn btn-md btn-primary'>Save</button>
                                                            </div>
                                                    </form>

                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>


                    <div class="tab-pane p-3" id="banner" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        @isset($banners)
                                            @if(count($banners))
                                            <div class="col-sm-12">
                                                <h3>Banner</h3>
                                                @foreach ($banners as $item)

                                                        <div class="banner-img-holder">
                                                                <a  class="image-popup-no-margins btn btn-xs"  href="{{getImageLink($item['content']['path'])}}"><img class="img-fluid img-thumbnail" src="{{getImageLink($item['content']['path'])}}" /></a>
                                                                <a class="btn btn-xs delete-banner" data-href='{{route('admin.site.banner.delete',$item['id'])}}' href="javascript:void"><i class="fa text-danger fa-trash-alt"></i> Delete</a>
                                                        </div>
                                                        <small>{{@$item['content']['caption']}}</small>


                                                @endforeach
                                            </div>
                                            @endif

                                        @endisset
                                        <div class="col-sm-12">
                                            <div class="card card-body">
                                                <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.site.banner')}}">
                                                    @csrf
                                                        <div class="form-group">

                                                            <label>Add photo</label>
                                                            <div class='banner-group'>
                                                                <div class="col-12">
                                                                    <div class="card">
                                                                        <div class="card-body">
                                                                            <h4 class="card-title">File Upload</h4>
                                                                            <label for="input-file-now">Upload Banner</label>
                                                                            <input required type="file" id="input-file-now" name="photo[]" class="dropify" />
                                                                            <label style="margin-top:10px">Head Caption</label>
                                                                            <input required class="form-control"  type="text" name='caption' />
                                                                            <label style="margin-top:10px">Small Caption</label>
                                                                            <input required class="form-control"  type="text" name='caption-small' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <button type="button" class='btn add-more3 btn-sm btn-outline-primary'>Add More</button>
                                                            </div>


                                                        </div>




                                                                <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                                </form>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>




            </div>


        </div>
    </div>



@endsection
@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
<script src="{{asset('assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.initialize.min.js')}}"></script>
<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>



<script>
        var editor = ClassicEditor.create( document.querySelector( '#terms_' ) ).then( editor => {
                            @isset($contents['terms']['editordata'])
                                editor.setData('{!! $contents['terms']['editordata'] !!}');
                            @endisset
                            console.log( editor );
                            }).catch( error => {
                                    console.error( error );
                                });


        var editor2 = ClassicEditor
                        .create( document.querySelector( '#personal_' ) )
                        .then( editor2 => {
                            @isset($contents['personal']['editordata'])
                                editor2.setData('{!! $contents['personal']['editordata'] !!}');
                            @endisset
                            console.log( editor2 );
                        } ).catch( error2 => {
                            console.error( error2 );
                            } );


        var editor3 = ClassicEditor
                        .create( document.querySelector( '#guarantee_' ) )
                        .then( editor3 => {
                                @isset($contents['guarantee']['editordata'])
                                    editor3.setData('{!! $contents['guarantee']['editordata'] !!}');
                                @endisset
                        console.log( editor3 );
                        } ).catch( error3 => {
                        console.error( error3 );
                        } );


        var editor4 = ClassicEditor
                        .create( document.querySelector( '#payments_' ) )
                        .then( editor4 => {
                                @isset($contents['payments']['editordata'])
                                    editor4.setData('{!! $contents['payments']['editordata'] !!}');
                                @endisset
                                console.log( editor4 );
                        } ).catch( error4 => {
                            console.error( error4 );
                        } );


    </script>
    <Script>
         no_of_banner = 1;
        value = 1000;
            $('.learn-more').click(function(){
                $('#learn-more').modal();
            });

            $('.delete-banner').click(function(){
               var url = $(this).attr('data-href');
                var answer = confirm('Are you sure? Click OK to continue');
                if(answer){
                    window.location.href = url;
                }
            });

            $('.add-faq').click(function(){

                var html = `<div id='remove${value}'><label>Question</label> <button type='button' class='btn btn-xs btn-danger remove-faq' target='#remove${value}' ><i class='fa fa-trash'></i></button>
                                <textarea required name='question${value}' class="form-control" rows="2"></textarea>
                            <label>Answer</label>
                                <textarea required name='answer${value}' class="form-control" rows="5"></textarea></div>`;
                value++;
                $('.add-more').append(html);
            });

            $('.add-step').click(function(){

var html = `<div id='remove${value}'><label>Step</label> <button type='button' class='btn btn-xs btn-danger remove-step' target='#remove${value}' ><i class='fa fa-trash'></i></button>
                <textarea required name='step${value}' class="form-control" rows="2"></textarea>
            <label>Answer</label>
                <textarea required name='answer${value}' class="form-control" rows="5"></textarea></div>`;
value++;
$('.add-more2').append(html);
});

            $(document).on('click', '.remove-faq', function(){
                var target = $(this).attr('target');
                $(target).remove();
            });

            $(document).on('click', '.remove-step', function(){
                var target = $(this).attr('target');
                $(target).remove();
            });


            $(document).ready(function() {
                        // Basic
        $('.dropify').dropify();


// Translated
$('.dropify-fr').dropify({
    messages: {
        default: 'Glissez-déposez un fichier ici ou cliquez',
        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
        remove: 'Supprimer',
        error: 'Désolé, le fichier trop volumineux'
    }
});

// Used events
var drEvent = $('#input-file-events').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
    alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
    console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
    e.preventDefault();
    if (drDestroy.isDropified()) {
        drDestroy.destroy();
    } else {
        drDestroy.init();
    }
})


});

$('.add-more3').click(function(){

    no_of_banner++;
    var html = ` <div class="col-12 banner-item">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h4 class="card-title">File Upload</h4>
                                                                        <label for="input-file-now">Upload Banner  <button class='btn btn-xs' type='button'><i  class='fa fa-trash remove-banner text-danger'> Delete</i></button></label>
                                                                        <input required type="file"  name="photo[]" class="dropify" />
                                                                        <label style="margin-top:10px">Head Caption</label>
                                                                            <input required class="form-control"  type="text" name='caption${no_of_banner}' />
                                                                            <label style="margin-top:10px">Small Caption</label>
                                                                            <input required class="form-control"  type="text" name='caption-small${no_of_banner}' />


                                                                    </div>
                                                                </div>
                                                            </div>`;
    $('.banner-group').append(html);
    $.initialize(".dropify", function() {
                    $(this).dropify();
                });

                $('.remove-banner').click(function(){
                    $(this).parents('div.banner-item').remove();


                });
});
    </script>
@endpush
