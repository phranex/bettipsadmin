@extends('layouts.admin')
@section('content')

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Dashboard</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </div>

</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daily Purchases</h4>
                <div class="text-right">
                <h2 class="font-light mb-0"><i class="ti-arrow-up text-success"></i> {{currency_format($total_amt_of_purchases_today)}}</h2>
                    <span class="text-muted">Todays Income</span>
                </div>
                {{-- <span class="text-success">80%</span>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Registrations</h4>
                <div class="text-right">
                    <h2 class="font-light mb-0"><i class="ti-arrow-up text-success"></i> {{$total_num_of_todays_users}}</h2>
                    <span class="text-muted">Todays User Reg.</span>
                </div>
                {{-- <span class="text-success">80%</span>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Users</h4>
                <div class="text-right">
                <h2 class="font-light mb-0"><i class="ti-arrow-up text-info"></i> {{$num_of_users}}</h2>
                    <span class="text-muted">Total Users (Active)</span>
                </div>
                {{-- <span class="text-info">30%</span>
                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Total Payments</h4>
                <div class="text-right">
                    <h2 class="font-light mb-0"><i class="ti-arrow-up text-purple"></i> {{currency_format($total_amt)}}</h2>
                    <span class="text-muted">Total Payments</span>
                </div>
                {{-- <span class="text-purple">60%</span>
                <div class="progress">
                    <div class="progress-bar bg-purple" role="progressbar" style="width: 60%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div> --}}
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->

    <!-- Column -->
</div>
<!-- Row -->
<!-- Row -->



<div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <h4 class="card-title">Users</h4>

                    </div>
                    <div class="table-responsive mt-5">
                        <table class="table stylish-table">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Membership Level</th>
                                    {{-- <th>Days Left</th> --}}
                                    <th>Registered</th>
                                    <th>Activated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($users))
                                    @if(count($users))
                                        @foreach($users as $user)
                                        <tr>
                                        <td style="width:50px;"><span class="round">{{strtoupper($user->name[0])}}</span></td>
                                            <td>
                                                <h6>{{ucwords($user->name)}}</h6>
                                                {{-- <small class="text-muted">Web Designer</small> --}}
                                            </td>
                                            <td>
                                                @if(count($user->memberships))
                                                    {{getLevel($user->memberships)}}
                                                @else
                                                    No level bought

                                                @endif
                                            </td>
                                            {{-- <td><span class="label label-light-success">21 days</span></td> --}}
                                            <td>{{getDateFormat($user->created_at, 'd-m-Y')}}</td>
                                            <td>
                                                @if($user->verified)
                                                    Yes
                                                @else
                                                    No
                                                @endif
                                            </td>
                                            <td>
                                            <a href='{{route('admin.users.show', $user->id)}}' class='btn btn-xs btn-info'>View</a>
                                            @if($user->blocked)
                                            <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-danger'>unblock</a>
                                            @else
                                                <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-danger'>Block</a>
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif

                                @endif


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex no-block">
                    <h4 class="card-title">Packages Statistics</h4>

                </div>
                <div class="table-responsive mt-5">
                    <table class="table stylish-table">
                        <thead>
                            <tr>
                                {{-- insert package name --}}
                                <th colspan="2">Package</th>
                                <th>Number of Users</th>
                                {{-- <th>%</th>
                                <th>Budget</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @isset($package_arr)
                                @if(count($package_arr))
                                    @foreach ($package_arr as $package => $value)
                                    <tr>

                                    <td colspan="2">{{$package}}</td>

                                    <td>{{$value}}</td>
                                        </tr>
                                    @endforeach

                                @endif
                            @endisset





                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Row -->
<!-- Row -->

<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul id="themecolors" class="mt-3">
                <li><b>With Light sidebar</b></li>
                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                <li class="d-block mt-4"><b>With Dark sidebar</b></li>
                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
            </ul>
            <ul class="mt-3 chatonline">
                <li><b>Chat option</b></li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="../assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                </li>
            </ul>
        </div>
    </div>
</div>


@endsection
