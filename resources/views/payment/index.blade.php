@extends('layouts.admin')
@section('content')


    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Payments</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Payments</li>
            </ol>
        </div>


    </div>


                <div class="row">
                    <div class="col-lg-10 m-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <h4 class="card-title">Payments made on platform</h4>

                                </div>
                                <div class="table-responsive mt-5">
                                    <table class="table stylish-table">
                                        <thead>
                                        <tr>
                                            <th colspan="2">S/N</th>
                                            <th>User</th>
                                            <th>Level</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>PayPal ID</th>
                                            <th>Reference Number</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($payments))
                                            @if(count($payments))
                                                @foreach ($payments as $payment)
                                                    <tr>
                                                        <td colspan="2">{{$loop->iteration}}</td>
                                                        <td><a href="{{route('admin.users.show',$payment->user_id)}}">{{$payment->user->name}}</a></td>
                                                        <td>{{$payment->level->name}}</td>
                                                        <td>{{currency_format($payment->amount)}}</td>
                                                        <td>{{$payment->status}}</td>
                                                        <td>{{$payment->paypal_subscription_id}}</td>
                                                        <td>{{$payment->reference_number}}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="9"><div class="text-center">Payment are yet to be made</div></td>
                                                </tr>



                                            @endif
                                        @endif
                                        {{$payments->links()}}

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>







@endsection
@push('scripts')

@endpush
