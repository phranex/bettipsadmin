@extends('layouts.admin')

@push('styles')
<style>
    .ck-editor__editable {
        min-height: 400px;
    }


    .banner-img-holder{
        width: 200px;
        display: inline-block;
    }

</style>

@endpush
@section('content')


<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Membership Plan</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Membership Plan</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
                <button class="btn float-right hidden-sm-down btn-success package-create "><i class="mdi mdi-plus-circle"></i> Create Package Plan</button>

            </div>

    </div>
    <div class="row">
            <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Packages</h4>
                            {{-- <h6 class="card-subtitle">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</h6> --}}
                            <div class="table-responsive">
                                <table class="table table-bordered no-wrap">
                                    <thead>
                                        <tr>
                                            <th>Package Name</th>
                                            <th>Description</th>
                                            <th>Paypal Package</th>
                                            <th>Show on Homepage</th>
                                            <th class="text-nowrap">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($packages))
                                            @if(count($packages))
                                                @foreach ($packages as $package)
                                                    <tr>
                                                            <td>{{$package->name}}</td>
                                                            <td>
                                                                {{$package->description}}
                                                                {{-- <div class="progress progress-xs margin-vertical-10 ">
                                                                    <div class="progress-bar bg-danger" style="width: 35%; height:6px;"></div>
                                                                </div> --}}
                                                            </td>
                                                            <td>
                                                                @if($package->product_id)
                                                                {{$package->product_id}}
                                                                @else
                                                                <a href="{{route('admin.package.store-paypal',$package->id)}}" class="btn btn-xs btn-outline-primary">Create on Paypal</a>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($package->show_packages  == 1)
                                                                    <i  style="font-size:12px !important" style="" class='fa fa-check text-success '> Showing</i>
                                                                @elseif($package->show_packages == 0)
                                                                        <i title="click to change" style="cursor:pointer;font-size:12px !important"  class='fa fa-times text-danger show'> Hidden</i>
                                                                    @endif
                                                                    <div style="display:none" class='show-actions'>
                                                                        <a href="{{route('admin.package.show-action',$package->id)}}" class='btn btn-success btn-xs'>Show</a>

                                                                    </div>

                                                            </td>
                                                            <td class="text-nowrap">
                                                            <a href="#" data-id='{{$package->id}}' data-name='{{$package->name}}' data-toggle="tooltip"  class='learn-more' data-original-title="Close"> <i class="fa fa-close text-danger"></i> Membership Levels</a>

                                                                <a href='#' data='{{$package}}' data-toggle="tooltip" class='btn btn-xs edit btn-outline-info' data-original-title="Edit"> <i class="fa fa-edit  mr-2"></i> </a>
                                                                <a href="#" data-id='{{$package->id}}' data-toggle="tooltip" class='btn btn-xs delete btn-outline-danger' data-original-title="Close"> <i class="fa fa-trash "></i> </a>
                                                            </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                    <tr>
                                                        <td class='alert alert-info' colspan="3"> No Package created yet</td>
                                                    </tr>

                                            @endif
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id='learn-more' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="package_name"></h4>
                        <div class="col-md-6 col-4 align-self-center">
                                <button class="btn float-right btn-success btn-sm add-level">Add New</button>

                            </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <div id='add-level' style="display:none" class='col-xs-12'>
                                    <form id='add-form' method='post' action="{{route('admin.level.store')}}" class="form-horizontal row mt-4">
                                            @csrf
                                            <div class="form-group col-12 col-md-4">
                                                <label>Membership Name</label>
                                                <input required type="text" id='level_name'  name="name" value="@if(old('name'))  {{old('how-it-works-subtitle')}} @endif" class="form-control" placeholder="">
                                            </div>
                                            <div class="form-group col-12 col-md-4">
                                                    <label>Period</label>
                                                    <input required type="number"  id='level_period' step="1" name="period" value="@if(old('name'))  {{old('how-it-works-subtitle')}} @endif" class="form-control" placeholder="">
                                            </div>
                                            {{-- <div class="form-group col-3">
                                                    <label>Percent</label>
                                                    <input required type="number"  name="percent" value="@if(old('name'))  {{old('how-it-works-subtitle')}} @endif" class="form-control" placeholder="">
                                            </div> --}}
                                            <div class="form-group col-12 col-md-4">
                                                    <label>Price</label>
                                                    <input required type="number" id="level_price"  name="price" value="@if(old('name'))  {{old('how-it-works-subtitle')}} @endif" class="form-control" placeholder="">
                                            </div>
                                            <div class="form-group col-12 col-md-8 m-auto">
                                                <label>Description of Level</label>
                                                <textarea  name='description' id='level_desc' rows="5" class="form-control">@if(old('description')) {{old('description')}} @endif</textarea>
                                            </div>
                                            <input type="hidden" name='package_id' id='package_id' />
                                            <div class='w-100 text-right'>
                                            <button type="submit" class="btn btn-success btn-sm m-3">Submit</button>
                                            </div>


                                        </form>

                            </div>
                            <div class="table-responsive">
                                    <table class="table table-striped no-wrap">
                                        <thead>
                                            <tr>
                                                <th>Membership Level</th>
                                                <th>Period</th>
                                                {{-- <th>Percent</th> --}}
                                                <th>Price</th>
                                                <th>Paypal Plan</th>
                                                <th class="text-nowrap">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id='level-holder'>




                                        </tbody>
                                    </table>
                                </div>
                    </div>
                    {{-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div> --}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>



    <div class="modal fade " tabindex="-1" role="dialog" id='package-create' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Package Creation</h4>

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <form id='package_form' method='post' action="{{route('admin.package.store')}}">
                                    @csrf
                                        <div class="form-group">
                                            <label>Package Name</label>
                                            <input required name='name' id='package_names' value="" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                                <label>Package Description</label>
                                                <textarea id='package_description' required name='description' class="form-control" rows="5"></textarea>

                                            </div>
                                            <input type='hidden' id='package_id2' name='package_id' />

                                                <button type="submit" class="btn btn-primary">Submit</button>

                                </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


@endsection
@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>

    <Script>
            $('.learn-more').click(function(){
                var package_id = $(this).attr('data-id');
                $('#level-holder').html('');
                var package_name = $(this).attr('data-name');
                $('#package_name').text(package_name);

                $('#package_id').val(package_id);
                //get all levels associated with this package
                 getLevels(package_id)
                $('#learn-more').modal();

            });
            level_desc = 2;
             editor = ClassicEditor.create( document.querySelector( '#level_desc' ), {

             } );
             console.log(editor);

                                console.log( level_desc );
            $('.show').click(function(){
                $('.show-actions').hide();
                $(this).siblings('.show-actions').toggle();
            });

            $('.edit').click(function(){
                var data = $(this).attr('data');
                data = JSON.parse(data);

                $('#package_names').val(data.name);
                $('#package_description').val(data.description);
                $('#package_id2').val(data.id);
                $('#package_form').attr('action', '{{route('admin.package.update')}}')
                $('#package-create').modal();


            });

            $(document).on('click', '.edit-level',function(){
                var data = $(this).attr('data');
                data = JSON.parse(data);


                $('#level_name').val(data.name);
                $('#level_desc').val(data.description);
                //console.log(ClassicEditor);
                editor.then( editor => {
                    editor.setData(data.description);
                })

                $('#level_price').val(data.price);
                $('#level_period').val(data.period);
                $('#add-form').attr('action', '{{route('admin.level.update')}}/'+ data.id);
                $('#add-level').show();
                $('.add-level').text('Cancel').addClass('btn-danger').removeClass('btn-success');



            });

            $('.delete').click(function(){
                var id = $(this).attr('data-id');
                var answer = confirm("Are you sure? If yes, click OK to continue");
                if(answer){
                window.location.href = "{{route('admin.package.delete')}}?id="+id;
                }
            });

            $('.package-create').click(function(){
                $('#package_names').val('');
                $('#package_description').val('');
                $('#package_id2').val('');
                $('#package_form').attr('action', '{{route('admin.package.store')}}')
                $('#package-create').modal();
            });

            $('.add-level').click(function(){
                $('#level_name').val('');
                $('#level_desc').val('');
                $('#level_price').val('');
                $('#level_period').val('');
                $('#add-form').attr('action', '{{route('admin.level.store')}}')
                $('#add-level').toggle();
                if($(this).text().toLowerCase() == 'add new' && $('#add-level').css('display') == 'block' ){
                    $(this).text('Cancel').addClass('btn-danger').removeClass('btn-success');
                }else if($('#add-level').css('display') == 'none' ){
                    $(this).text('Add New').addClass('btn-success').removeClass('btn-danger');
                }
            });

            function getLevels(id){
                var url = '{{route('admin.package.get-levels')}}' + '?id=' +id;
                $.ajax({
                    url: url,
                    success: function(result){
                        if(result.status){
                            var html = '';
                            console.log(result);
                            if(result.data.length){
                                for(i = 0; i < result.data.length; i++){
                                  if(result.data[i].plan_id == null) var plan = "<a href={{route('admin.level.store-paypal')}}/"+result.data[i].id+ "class='btn btn-xs btn-outline-primary'>Create on Paypal</a>";
                                  else var plan = result.data[i].plan_id
                                html += `<tr>
                                                <td>${result.data[i].name}</td>
                                                <td>
                                                        ${result.data[i].period} days
                                                </td>


                                                <td>${result.data[i].price}</td>
                                                <td>${plan}</td>
                                                <td class="text-nowrap">
                                                    <button data='${JSON.stringify(result.data[i])}' class='btn btn-xs edit-level btn-outline-info'> <i class="fa fa-edit text-inverse mr-2"></i> </a>

                                                </td>
                                            </tr>`;
                                }

                            }else{
                                html += "<tr><td colspan='5'><div class='alert text-center'>No membership level created for the package yet</div></td></tr>";
                            }
                            $('#level-holder').html(html);

                        }
                    },
                    error: function(){
                        $.toast({
                            heading: 'Error',
                            text: 'An unexpected error occurred. Please try again',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500

                        });
                    }
                });
            }

    </script>
@endpush
