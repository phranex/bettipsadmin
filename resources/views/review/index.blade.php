@extends('layouts.admin')
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/css/lightbox.min.css" rel="stylesheet">
<Style>
        .grid-item { width: 200px; border:  3px solid #ee;}
        .grid-item img{
            padding: 5px;
            border:  3px solid #eee;
        }
        .grid-item img:hover{
            transform: scale(1.1);
            transition: 1s;
        }
        .grid-item--width2 img{
            padding: 5px;
            border:  3px solid #ee;
        }
    .grid-item--width2 { width: 400px; }
    </style>

@endpush
@section('content')

<div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor mb-0 mt-0">Reviews</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Reviews/Instagram Photo</li>
            </ol>
        </div>


    </div>

    <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="no-block">
                            <h4 class="card-title">Reviews/Instagram Photo <button class="btn upload-review btn-sm btn-outline-primary">Upload Review</button> <button class="btn upload-instagram-photo btn-sm btn-outline-primary">Upload Instagram Photo</button></h4>
                            <div class="grid">
                                @isset($reviews)
                                    @if(count($reviews))
                                        @foreach($reviews as $review)
                                            @if($loop->iteration%2 || $loop->iteration % 4 == 0)
                                        <div class="grid-item">
                                                <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="@if($review->isInstagram == 1) {{$review->link}} @else {{getImageLink($review->path)}} @endif"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>
                                        <button data-href='{{route('admin.review.delete',$review->id)}}' class='btn delete-review btn-xs'><i class="fa fa-trash-alt  text-danger"></i>  Delete</button>
                                        @if($review->isInstagram == 1) <i style="font-size:10px">Instagram Photo</i>  @endif
                                    </div>
                                        @else
                                        <div class="grid-item grid-item--width2">
                                                <a  data-lightbox="portfolio" class="image-popup-no-margins btn btn-xs text-white"  href="@if($review->isInstagram == 1) {{$review->link}} @else {{getImageLink($review->path)}} @endif"><img src="{{getImageLink($review->path)}}" class="d-block w-100 img-fluid" alt="..."></a>
                                                <button data-href='{{route('admin.review.delete',$review->id)}}' class='btn delete-review btn-xs'><i class="fa fa-trash-alt  text-danger"></i>  Delete</button>
                                                @if($review->isInstagram == 1) <i style="font-size:10px">Instagram Photo</i>  @endif

                                        </div>
                                        @endif



                                        @endforeach
                                    @else
                                    {{-- <div class="alert text-center ">

                                            No reviews Yet
                                        </div> --}}

                                    @endif
                                @else
                                    {{-- <div class="alert text-center alert-default">

                                        No reviews Yet
                                    </div> --}}
                                @endisset


                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="modal fade " tabindex="-1" role="dialog" id='add-review' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Upload Reviews</h4>

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                                <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.review.store')}}">
                                        @csrf
                                            <div class="form-group">


                                                <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <h4 class="card-title">File Upload</h4>
                                                                <label for="input-file-now">Upload Review</label>
                                                                <input required type="file" id="input-file-now" multiple name="photo[]" class="dropify" />
                                                            </div>
                                                        </div>
                                                    </div>

                                            </div>




                                                    <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                    </form>
                        </div>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
        </div>


        <div class="modal fade " tabindex="-1" role="dialog" id='add-instagram' aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel">Upload Instagram Photo</h4>

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.review.store')}}">
                                    @csrf
                                        <div class="form-group">


                                            <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <h4 class="card-title">File Upload</h4>
                                                            <input type="hidden" value='1' name="instagram" />
                                                            <label for="input-file-now">Upload Photo</label>
                                                            <input required type="file" id="input-file-now"  name="photo[]" class="dropify" />

                                                            <div class="form-group" style="margin-top:10px">
                                                                <label for="input-file-now">Full Name</label>
                                                                <input type="text" class="form-control" name='fullname' required />
                                                            </div>

                                                            <div class="form-group" style="margin-top:10px">
                                                                <label for="input-file-now">Title</label>
                                                                <input type="text" class="form-control" name='title' required />
                                                            </div>
                                                            <div class="form-group" style="margin-top:10px">
                                                                <label for="input-file-now">Review</label>
                                                               <textarea name="description" class="form-control" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                        </div>




                                                <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                </form>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>

@endsection

@push('scripts')

<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.0/js/lightbox.min.js"></script>

<script>
        $('.grid').masonry({
          // options
          itemSelector: '.grid-item',
          columnWidth: 200
        });

        $('.upload-review').click(function(){
            $('#add-review').modal();
        });

        $('.upload-instagram-photo').click(function(){
            $('#add-instagram').modal();
        });
        $('.delete-review').click(function(){
               var url = $(this).attr('data-href');
                var answer = confirm('Are you sure? Click OK to continue');
                if(answer){
                    window.location.href = url;
                }
            });
        </script>

@endpush
