@if(count($errors) > 0)
<div  class='invisibe light-border messages alert' style='background:#ddd'>
         <a href="#" class="close "  data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
     <div id='error'>
         <ul>
        @foreach($errors->all() as $error)



                <li class='text-danger'>
                    {{$error}}
                </li>

        @endforeach
        </ul>
      </div>
</div>

@endif


@if(session('success'))
     <div id='' class='alert alert-success text-center invisibl messages alert'>
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <div id='successMess'>
                <p>{{session('success')}}</p>
            </div>
        </div>

@endif


@if(session('error'))
     <div class='alert bg-danger invisibl light-border no-bd-rad messages fad in'>
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <div id='errorMess'>
                <p>{{session('error')}}</p>
            </div>
        </div>

@endif
